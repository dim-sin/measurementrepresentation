#pragma once

#include "Point.h"

#include <list>

struct Data
{
	std::list<Point> points;

	double maxValue;
	double minValue;
};

#include "DataLoader.h"

#include "Point.h"
#include "Data.h"
#include "LoaderException.h"

#include <QFile>
#include <QString>
#include <QByteArray>
#include <QMessageBox>

#include <optional>
#include <limits>


using namespace std;

const char CDataLoader::c_headerTag = '#';
const string CDataLoader::c_wrongFormat = "Wrong data format";
const string CDataLoader::c_fileIsMissing = "File is missing";
const string CDataLoader::c_fileOpenError = "File open error";
const string CDataLoader::c_noDataMessage = "No data";


namespace
{
	void updateMaxMin(double valueForCheck, double& max, double& min)
	{
		if (valueForCheck < min)
			min = valueForCheck;
		else if (valueForCheck > max)
			max = valueForCheck;
	}
}

CDataLoader::CDataLoader(const QString& fileName) : m_fileName(fileName)
{

}

Data CDataLoader::load() const
{
	QFile file(m_fileName);

	if (!file.exists())
		throw CLoaderException(c_fileIsMissing);
	if (!file.open(QIODevice::ReadOnly | QIODevice::Text))
		throw CLoaderException(c_fileOpenError);

	Data res;
	double min = std::numeric_limits<double>::max();
	double max = std::numeric_limits<double>::min();
	while (!file.atEnd())
	{
		auto str = file.readLine();
		auto point = getPoint(str);
		if (point.has_value())
		{
			res.points.push_back(point.value());
			updateMaxMin(point->value, max, min);
		}
	}

	if (res.points.empty())
		throw CLoaderException(c_noDataMessage);

	res.maxValue = max;
	res.minValue = min;

	return res;
}

optional<Point> CDataLoader::getPoint(const QByteArray& str) const
{
	if (str.isEmpty())
		return {};
	if (isHeader(str))
		return {};

	return parsePoint(str);
}

bool CDataLoader::isHeader(const QByteArray& str) const
{
	assert(!str.isEmpty());
	return str.at(0) == c_headerTag;
}

optional<Point> CDataLoader::parsePoint(const QByteArray& str) const
{
	assert(!str.isEmpty());

	QString string(str);
	auto rawData = string.split(' ', QString::SkipEmptyParts);

	if (rawData.size() != 2)
		return {};// throw CLoaderException(c_wrongFormat + ": " + str.data());

	optional<Point> out;
	bool ok1 = true;
	bool ok2 = true;
	out = Point(rawData.at(0).toDouble(&ok1), rawData.at(1).toDouble(&ok2));
	if(!ok1 || !ok2)
		throw CLoaderException(c_wrongFormat + ": " + str.data());
	
	return out;
}

void CDataLoader::displayMessage(const QString& message) const
{
	QMessageBox messageBox;
	messageBox.critical(0, "Error", message);
	messageBox.setFixedSize(500, 200);
}


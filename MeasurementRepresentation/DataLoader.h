#pragma once

#include <QString>

#include <string>
#include <optional>
#include <list>


class Point;
class Data;

class CDataLoader
{
public:
	CDataLoader(const QString& fileName);

	Data load() const;

	static const char c_headerTag;
	static const std::string c_wrongFormat;
	static const std::string c_fileIsMissing;
	static const std::string c_fileOpenError;
	static const std::string c_noDataMessage;

private:
	void displayMessage(const QString& message) const;
	bool isHeader(const QByteArray& str) const;
	std::optional<Point> parsePoint(const QByteArray& str) const;
	std::optional<Point> getPoint(const QByteArray& str) const;

private:
	QString m_fileName;
};
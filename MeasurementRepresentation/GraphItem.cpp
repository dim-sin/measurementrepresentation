#include "GraphItem.h"

#include "Point.h"

#include <QPainter>
#include <QGraphicsView>


CGraphItem::CGraphItem(const QGraphicsView* pParentView, const Data& data, double k1, double k2, const QPen& pen) :
	QGraphicsItem(),
	m_pParentView(pParentView),
	m_data(data),
	m_k1(k1),
	m_k2(k2),
	m_pen(pen)
{
}

void CGraphItem::paint(QPainter* painter, const QStyleOptionGraphicsItem* option, QWidget* widget)
{
	if (m_data.points.empty())
		return;

	painter->setPen(m_pen);
	auto p1 = QPointF(m_data.points.front().timestamp * m_k1, m_data.points.front().value * m_k2);
	for (auto it = ++m_data.points.begin(); it != m_data.points.end(); ++it)
	{
		auto p2 = QPointF(it->timestamp * m_k1, it->value * m_k2);

		if(m_pParentView->mapFromScene(p1) != m_pParentView->mapFromScene(p2))
			painter->drawLine(p1, p2);
		
		p1 = p2;
	}
}

QRectF CGraphItem::boundingRect() const
{
	return QRectF(QPointF(m_data.points.front().timestamp * m_k1, m_data.minValue * m_k2), QPointF(m_data.points.back().timestamp * m_k1, m_data.maxValue * m_k2));
}




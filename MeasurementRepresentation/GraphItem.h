#pragma once
#include <QGraphicsItem>
#include <QPen>

#include "Data.h"

#include <list>


class CGraphItem : public QGraphicsItem
{
public:
	CGraphItem(const QGraphicsView* parentView, const Data& points, double k1, double k2, const QPen& pen);
	void paint(QPainter* painter, const QStyleOptionGraphicsItem* option, QWidget* widget = nullptr) override;
	QRectF boundingRect() const override;

private:
	const QGraphicsView* m_pParentView;

	Data m_data;
	double m_k1;
	double m_k2;
	QPen m_pen;
};


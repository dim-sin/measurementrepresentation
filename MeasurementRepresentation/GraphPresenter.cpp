#include "GraphPresenter.h"

#include "Data.h"
#include "GraphView.h"


CGraphPresenter::CGraphPresenter(CGraphView* pView) : m_pView(pView)
{
}

CGraphPresenter::~CGraphPresenter()
{
}

void CGraphPresenter::display(const Data& data)
{
	auto tMin = data.points.front().timestamp;
	auto tMax = data.points.back().timestamp;
	auto vMin = data.minValue;
	auto vMax = data.maxValue;
	auto k1 = 1000 / (tMax - tMin);
	auto k2 = 1000 / (vMax - vMin);

	m_pView->drawGrid(data.points.front().timestamp*k1, data.points.back().timestamp*k1, data.minValue*k2, data.maxValue*k2);
	m_pView->drawGraph(data, k1, k2);
}
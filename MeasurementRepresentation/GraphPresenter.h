#pragma once

#include "Data.h"


class CGraphView;

class CGraphPresenter
{
public:
	CGraphPresenter(CGraphView* pView);
	~CGraphPresenter();

	void display(const Data& data);

private:
	CGraphView* m_pView;
	Data m_data;
};

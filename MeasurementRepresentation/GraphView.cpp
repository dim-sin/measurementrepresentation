#include "GraphView.h"
#include "Point.h"
#include "GraphItem.h"

#include <QPen>
#include <QResizeEvent>
#include <QGraphicsTextItem>

#include <list>


const int CGraphView::c_lineCount = 10;

CGraphView::CGraphView(QWidget* parent)
	: QGraphicsView(parent),
	m_graphPen(Qt::blue, 1, Qt::SolidLine),
	m_gridPen(Qt::gray, 1, Qt::DotLine)
{
	m_graphPen.setCosmetic(true);
	m_gridPen.setCosmetic(true);

	m_pScene = new QGraphicsScene(parent);

	//setRenderHints(QPainter::Antialiasing);
	setHorizontalScrollBarPolicy(Qt::ScrollBarAlwaysOff);
	setVerticalScrollBarPolicy(Qt::ScrollBarAlwaysOff);
	setAlignment(Qt::AlignLeft | Qt::AlignTop);
	setSizePolicy(QSizePolicy::Expanding, QSizePolicy::Expanding);

	setScene(m_pScene);
}

CGraphView::~CGraphView()
{
}

void CGraphView::clearItem(QGraphicsItem*& pItem) const
{
	if (!pItem)
		return;

	m_pScene->removeItem(pItem);
	delete pItem;
	pItem = nullptr;
}

void CGraphView::drawGrid(double tMin, double tMax, double vMin, double vMax)
{
	QGraphicsItem* pItem = dynamic_cast<QGraphicsItem*>(m_pGridGroup);
	clearItem(pItem);
	m_pGridGroup = new QGraphicsItemGroup();
	m_pScene->addItem(m_pGridGroup);

	drawGridLineVertical(m_pGridGroup, tMin, tMax, vMin, vMax, m_gridPen);
	drawGridLineHorizontal(m_pGridGroup, tMin, tMax, vMin, vMax, m_gridPen);

	fitInView(m_pScene->itemsBoundingRect(), Qt::IgnoreAspectRatio);
	setSceneRect(m_pScene->itemsBoundingRect());
}

void CGraphView::drawGridLineVertical(QGraphicsItemGroup* pGroup, double tMin, double tMax, double vMin, double vMax, const QPen& pen)
{
	auto x1 = tMin;
	auto y1 = vMin;

	auto x2 = tMax;
	auto y2 = vMax;

	const auto values = getLinePlaces(x1, x2, c_lineCount);
	for (const auto& v : values)
	{
		auto pLineItem = new QGraphicsLineItem(v, y1, v, y2);
		pLineItem->setPen(pen);
		pGroup->addToGroup(pLineItem);
	}
}

void CGraphView::drawGridLineHorizontal(QGraphicsItemGroup* pGroup, double tMin, double tMax, double vMin, double vMax, const QPen& pen)
{
	auto x1 = tMin;
	auto y1 = vMin;

	auto x2 = tMax;
	auto y2 = vMax;

	const auto values = getLinePlaces(y1, y2, c_lineCount);

	for (const auto& v : values)
	{
		auto pLineItem = new QGraphicsLineItem(x1, v, x2, v);
		pLineItem->setPen(pen);
		pGroup->addToGroup(pLineItem);
	}
}

std::vector<double> CGraphView::getLinePlaces(double from, double to, int count) const
{
	double step = (to - from) / count;
	std::vector<double> out;
	out.reserve(count);
	for (int i = 0; i <= count; i++)
		out.push_back(from + step * i);

	return out;
}

void CGraphView::drawGraph(const Data& data, double k1, double k2)
{
	assert(!data.points.empty());
	clearItem(m_pGraphItem);

	m_pGraphItem = new CGraphItem(this, data, k1, k2, m_graphPen);
	m_pScene->addItem(m_pGraphItem);
	
	fitInView(m_pScene->itemsBoundingRect(), Qt::IgnoreAspectRatio);
	setSceneRect(m_pScene->itemsBoundingRect());
}

void CGraphView::resizeEvent(QResizeEvent* event)
{
	QGraphicsView::resizeEvent(event);

	fitInView(m_pScene->itemsBoundingRect(), Qt::IgnoreAspectRatio);
	setSceneRect(m_pScene->itemsBoundingRect());
}
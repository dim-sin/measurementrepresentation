#pragma once

#include "Data.h"

#include <QGraphicsView>


struct ScreenRectView;
struct Point;
class CGraphItem;

class CGraphView : public QGraphicsView
{
	Q_OBJECT

public:
	CGraphView(QWidget* parent);
	~CGraphView();

	static const int c_lineCount;

	void drawGraph(const Data& points, double k1, double k2);
	void drawGrid(double tMin, double tMax, double vMin, double vMax);
	
	void resizeEvent(QResizeEvent* event) Q_DECL_OVERRIDE;

private:
	void drawGridLineHorizontal(QGraphicsItemGroup* pGroup, double tMin, double tMax, double vMin, double vMax, const QPen& pen);
	void drawGridLineVertical(QGraphicsItemGroup* pGroup, double tMin, double tMax, double vMin, double vMax, const QPen& pen);
	std::vector<double> getLinePlaces(double from, double to, int count) const;
	void clearItem(QGraphicsItem* &pGroup) const;

	QPen m_graphPen;
	QPen m_gridPen;

	QGraphicsScene* m_pScene;
	QGraphicsItemGroup* m_pGridGroup = nullptr;
	QGraphicsItem* m_pGraphItem = nullptr;
};

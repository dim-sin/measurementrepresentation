#pragma once

#include <exception>
#include <string>


class CLoaderException : public std::exception
{
public:
	CLoaderException(const std::string& message): m_message(message)
	{

	}

	virtual char const* what() const
	{
		auto mes = m_message.c_str();
		return mes;
	}

private:
	std::string m_message;
};

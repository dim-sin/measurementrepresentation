#include "MeasurementRepresentation.h"

#include "DataLoader.h"
#include "Data.h"
#include "LoaderException.h"

#include <QMessageBox>
#include <QFileDialog>
#include <QSettings>

#include <memory>


const QString MeasurementRepresentation::c_lastDataDirKey = "dirKey";
const QString MeasurementRepresentation::c_fileErrorMessage = "File loading error";

MeasurementRepresentation::MeasurementRepresentation(QWidget* parent)
	: QMainWindow(parent)
{
	ui.setupUi(this);
	m_pGraphPresenter = std::make_unique<CGraphPresenter>(ui.graphView);
	setWindowState(Qt::WindowMaximized);
}

void MeasurementRepresentation::drawData()
{
	auto data = load();
	if (!data.points.empty())
		m_pGraphPresenter->display(data);
}

Data MeasurementRepresentation::load()
{
	const auto path = getFilePath();
	if (path.isEmpty())
		return {};

	CDataLoader loader(path);

	try
	{
		return loader.load();
	}
	catch (const CLoaderException& ex)
	{
		displayMessage(ex.what());
	}
	catch (...)
	{
		displayMessage(c_fileErrorMessage);
	}

	return {};
}

QString MeasurementRepresentation::getFilePath() const
{
	const auto fileFilter = QString("%0 (*.ssd *.rsd)").arg(tr("ttOpenDataFileType"));
	auto path = QFileDialog::getOpenFileName(nullptr, "Choose data file", currentDir(), fileFilter);
	if (!path.isEmpty())
		saveCurrentDir(path);

	return path;
}

void MeasurementRepresentation::displayMessage(const QString& message) const
{
	QMessageBox messageBox;
	messageBox.setFixedSize(500, 200);
	messageBox.critical(0, "Error", message);
}

QString MeasurementRepresentation::currentDir() const
{
	QSettings settings;
	return settings.value(c_lastDataDirKey).toString();
}

void MeasurementRepresentation::saveCurrentDir(const QString& filePath) const
{
	QDir dir;
	QSettings settings;
	settings.setValue(c_lastDataDirKey, dir.absoluteFilePath(filePath));
}
#pragma once

#include <QtWidgets/QMainWindow>

#include "ui_MeasurementRepresentation.h"

#include "GraphPresenter.h"


class Data;
class CGraphView;

class QProgressBar;

class MeasurementRepresentation : public QMainWindow
{
    Q_OBJECT

public:
    MeasurementRepresentation(QWidget *parent = Q_NULLPTR);

    static const QString c_lastDataDirKey;
    static const QString c_fileErrorMessage;
    
public slots:
    void drawData();

private:
    Data load();
    QString getFilePath() const;
    void displayMessage(const QString&) const;
    QString currentDir() const;
    void saveCurrentDir(const QString& filePath) const;

private:
    Ui::MeasurementRepresentationClass ui;

    std::unique_ptr<CGraphPresenter> m_pGraphPresenter;
    CGraphView* m_pGraphView;
};

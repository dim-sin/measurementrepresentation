#pragma once

struct Point
{
	Point(const double& t, const double& v): timestamp(t), value(v)
	{

	}

	double timestamp;
	double value;	
};